package com.ludev.estoriabackend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class EstoriaBackendApplication {

	static Logger logger = LoggerFactory.getLogger(EstoriaBackendApplication.class);

	public static void main(String[] args) {
		logger.info("Démarrage de l'application");
		SpringApplication.run(EstoriaBackendApplication.class, args);
		logger.info("L''application a bien démarré");
	}
}
