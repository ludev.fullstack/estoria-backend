package com.ludev.estoriabackend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.ludev.estoriabackend.api.UtilisateurApi;

@RestController
public class UtilisateurController implements UtilisateurApi{

    @Override
    public ResponseEntity<String> getUser() {
        return ResponseEntity.ok("EIGENMANN");
    }
    
}
