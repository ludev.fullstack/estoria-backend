package com.ludev.estoriabackend.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.models.media.MediaType;

@Tag(name = "Utilisateur", description = "Endpoints pour la gestion des utilisateurs")
@RequestMapping("/api/user")
public interface UtilisateurApi {
    
    @Operation(description = "Méthode permettant de récupérer un utilisateur.")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "L'utilisateur a été récupéré", content = @Content(mediaType = "application/json")),
        @ApiResponse(responseCode = "400", description = "L'utilisateur n'a pas été récupéré", content = @Content),
        @ApiResponse(responseCode = "403", description = "Erreur d'autorisation", content = @Content),
    })
    @GetMapping("/")
    public ResponseEntity<String> getUser();
}
